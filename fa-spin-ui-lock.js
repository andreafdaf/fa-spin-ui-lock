(function($) {
	$.extend({ 
		uiLock: function(opacity){
			if(opacity>1) opacity=1
			if(opacity<0) opacity=0
			content='<div style="text-align: center; height: 100px; width: 100px; position: fixed; top:0; bottom: 0; left: 0; right: 0; margin: auto;">'+'<i class="fa fa-refresh fa-spin" style="font-size:100px;color:black;"></i></div>'
			if($('#uiLockId').length) return
			$('<div></div>').attr('id', 'uiLockId').css({
				'position': 'fixed',
				'top': 0,
				'left': 0,
				'z-index': 1029,
				'width':'10000px',
				'height':'10000px',
				'color':'white',
				'background-color':'rgba(255,255,255,'+opacity+')',
				'cursor':'wait !important'
			}).html(content).appendTo('body');
		},
		uiUnlock: function(){
			$('#uiLockId').remove();
		}
	});
})(jQuery);
